﻿using System;
using System.Collections.Generic;
using System.Text;
using Izio.Stockr.Core.Interfaces;
using Microsoft.Data.Sqlite;
using NPoco;

namespace Izio.Stockr.Core.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDatabase _database;

        public UnitOfWork(string connectionString)
        {
            _database = new Database(connectionString, DatabaseType.SQLite, SqliteFactory.Instance);
            _database.BeginTransaction();

            ProductPages = new ProductPageRepository(_database);
        }

        #region IDisposable

        public void Dispose()
        {
            _database.AbortTransaction();
        }

        #endregion

        #region Implementation of IUnitOfWork

        public IProductPageRepository ProductPages { get; }

        public void Save()
        {
           _database.CompleteTransaction();
        }

        #endregion
    }
}
