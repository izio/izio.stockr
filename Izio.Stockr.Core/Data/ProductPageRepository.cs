﻿using Izio.Stockr.Core.Interfaces;
using Izio.Stockr.Core.Models;
using NPoco;
using System.Collections.Generic;

namespace Izio.Stockr.Core.Data
{
    public class ProductPageRepository : IProductPageRepository
    {
        #region private properties

        private readonly IDatabase _database;

        #endregion

        #region constructors

        public ProductPageRepository(IDatabase database)
        {
            _database = database;
        }

        #endregion

        #region IProductPageRepository

        public List<ProductPage> Get()
        {
            return _database.Fetch<ProductPage>();
        }

        public void Add(ProductPage productPage)
        {
            _database.Insert(productPage);
        }

        public void Delete(ProductPage productPage)
        {
            _database.Delete(productPage);
        }

        #endregion
    }
}