﻿using Izio.Stockr.Core.Interfaces;
using NPoco;
using System.Text.Json.Serialization;

namespace Izio.Stockr.Core.Models
{
    [PrimaryKey("Id")]
    public abstract class IntegerEntity : IDatabaseEntity<int>
    {
        #region IDatabaseEntity<int>
        
        public int Id { get; internal set; }

        [Ignore]
        [JsonIgnore]
        public bool IsNew => Id == 0;

        #endregion
    }
}
