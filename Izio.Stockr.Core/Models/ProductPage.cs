﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.Json.Serialization;
using NPoco;

namespace Izio.Stockr.Core.Models
{
    /// <summary>
    /// A product page that will be checked for stock
    /// </summary>
    [TableName("ProductPages")]
    public class ProductPage : IntegerEntity, INotifyPropertyChanged
    {
        #region private properties

        private bool _stock;
        private bool _checking;

        #endregion

        #region public properties

        [Column("Name")]
        public string Name { get; set; }

        [Column("Type")]
        public string Type { get; set; }

        [Column("Address")]
        public string Address { get; set; }

        [ResultColumn]
        public string Icon
        {
            get
            {
                var x = new Uri(Address);
                return $"{x.GetLeftPart(UriPartial.Authority)}/favicon.ico";
            }
        }

        [Column("Node")]
        public string Node { get; set; }

        [Column("Match")]
        public string Match { get; set; }

        [ResultColumn]
        public bool Checking
        {
            get => _checking;
            set
            {
                _checking = value;

                RaisePropertyChanged();
            }
        }

        [ResultColumn]
        public bool Stock
        {
            get => _stock;
            set
            {
                _stock = value;

                RaisePropertyChanged();
            }
        }

        #endregion

        #region overrides
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            return obj is ProductPage p && Equals((ProductPage)obj);
        }

        public bool Equals(ProductPage obj)
        {
            return obj.Name == Name && obj.Address == Address;
        }
        
        public override int GetHashCode()
        {
            unchecked
            {
                const int hashingBase = (int)2166136261;
                const int hashingMultiplier = 16777619;

                var hash = hashingBase;

                hash = (hash * hashingMultiplier) ^ (Name?.GetHashCode() ?? 0);
                hash = (hash * hashingMultiplier) ^ (Address?.GetHashCode() ?? 0);
                hash = (hash * hashingMultiplier) ^ (Node?.GetHashCode() ?? 0);
                hash = (hash * hashingMultiplier) ^ (Match?.GetHashCode() ?? 0);

                return hash;
            }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged([CallerMemberName] string caller = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(caller));
        }

        #endregion
    }
}