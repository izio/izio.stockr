﻿namespace Izio.Stockr.Core.Interfaces
{
    public interface IDatabaseEntity<T>
    {
        /// <summary>
        /// the unique id of the object
        /// </summary>
        T Id { get; }

        /// <summary>
        /// indicates whether the object has been persisted to the database
        /// </summary>
        bool IsNew { get; }
    }
}
