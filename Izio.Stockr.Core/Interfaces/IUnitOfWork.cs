﻿using System;

namespace Izio.Stockr.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IProductPageRepository ProductPages { get; }

        void Save();
    }
}