﻿using Izio.Stockr.Core.Models;
using System.Collections.Generic;

namespace Izio.Stockr.Core.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IProductPageRepository
    {
        #region methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<ProductPage> Get();

        /// <summary>
        /// 
        /// </summary>
        /// <param name=""></param>
        /// <param name="productPage"></param>
        void Add(ProductPage productPage);

        /// <summary>
        /// 
        /// </summary>
        /// <param name=""></param>
        /// <param name="productPage"></param>
        void Delete(ProductPage productPage);

        #endregion
    }
}
