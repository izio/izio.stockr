﻿using HtmlAgilityPack;
using Izio.Stockr.Core.Data;
using Izio.Stockr.Core.Interfaces;
using Izio.Stockr.Core.Models;
using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.System;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Izio.Stockr
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        #region private properties

        private readonly HttpClient _client;
        private readonly string _connectionString;
        private readonly DispatcherTimer _timer;
        private int _loopCount;
        private int _loopLimit;
        private int _interval;

        #endregion

        #region public properties

        public ObservableCollection<ProductPage> ProductPages;

        #endregion

        #region constructors

        public MainPage()
        {
            InitializeComponent();

            _client = new HttpClient();
            _connectionString = $"Data Source={ApplicationData.Current.LocalFolder.Path}\\sqliteSample.db";
            _timer = new DispatcherTimer();
            _loopCount = 0;
            _loopLimit = 0;

            ComboInterval.SelectedIndex = 0;
            ComboIterations.SelectedIndex = 0;

            ProductPages = new ObservableCollection<ProductPage>();
        }

        #endregion

        #region overrides

        /// <summary>
        /// loads product pages and enables stock checking button
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                var pages = unitOfWork.ProductPages.Get();

                foreach (var page in pages.OrderBy(p => p.Name))
                {
                    ProductPages.Add(page);
                }

                ProductPages.CollectionChanged += ProductPages_CollectionChanged;

                if (ProductPages.Count > 0)
                {
                    ButtonStockCheck.IsEnabled = true;
                }
            }
        }

        #endregion

        #region events

        /// <summary>
        ///  disables stock checking when there are no product pages to check
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductPages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (ProductPages.Count > 0)
            {
                ButtonStockCheck.IsEnabled = true;
            }
        }

        /// <summary>
        /// enables the delete option on long press
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPages_OnHolding(object sender, HoldingRoutedEventArgs e)
        {
            var tappedItem = (UIElement)e.OriginalSource;
            var senderElement = sender as FrameworkElement;
            var flyoutBase = FlyoutBase.GetAttachedFlyout(senderElement);

            flyoutBase.ShowAt(tappedItem, new FlyoutShowOptions { Position = e.GetPosition(tappedItem) });
        }

        /// <summary>
        /// enables the delete option on right click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPages_OnRightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            var tappedItem = (UIElement)e.OriginalSource;
            var senderElement = sender as FrameworkElement;
            var flyoutBase = FlyoutBase.GetAttachedFlyout(senderElement);

            flyoutBase.ShowAt(tappedItem, new FlyoutShowOptions { Position = e.GetPosition(tappedItem) });
        }

        /// <summary>
        /// deletes the selected product page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuFlyoutItemDelete_OnClick(object sender, RoutedEventArgs e)
        {
            var item = sender as MenuFlyoutItem;
            var productPage = item.DataContext as ProductPage;

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                unitOfWork.ProductPages.Delete(productPage);
                unitOfWork.Save();

                ProductPages.Remove(productPage);
            }
        }

        /// <summary>
        /// opens the product page in the browser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void gridPages_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (e.ClickedItem is ProductPage)
            {
                var productPage = e.ClickedItem as ProductPage;

                await Launcher.LaunchUriAsync(new Uri(productPage.Address));
            }
        }

        /// <summary>
        /// starts or stops the stock checking process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void buttonStockCheck_Click(object sender, RoutedEventArgs e)
        {
            if (_timer.IsEnabled)
            {
                _timer.Stop();

                ButtonStockCheck.Content = "Start";
                ComboInterval.IsEnabled = true;
                ComboIterations.IsEnabled = true;
                StackStatistics.Visibility = Visibility.Collapsed;
            }
            else
            {
                _loopCount = 1;
                _interval = Convert.ToInt32(ComboInterval.SelectedValue);
                _loopLimit = Convert.ToInt32(ComboIterations.SelectedValue);
                _timer.Interval = TimeSpan.FromMinutes(_interval);
                _timer.Tick += Timer_Tick;
                _timer.Start();

                ButtonStockCheck.Content = "Stop";
                ComboInterval.IsEnabled = false;
                ComboIterations.IsEnabled = false;
                StackStatistics.Visibility = Visibility.Visible;
                TextLoopCount.Text = _loopCount.ToString();
                TextLastRun.Text = DateTime.Now.ToShortTimeString();
                TextNextRun.Text = DateTime.Now.AddMinutes(_interval).ToShortTimeString();

                CheckStock();
            }

        }

        /// <summary>
        /// adds a new product page with teh details entered
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            var productPage = new ProductPage
            {
                Name = TextName.Text,
                Type = TextType.Text,
                Address = TextAddress.Text,
                Node = TextNode.Text,
                Match = TextMatch.Text
            };

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                unitOfWork.ProductPages.Add(productPage);
                unitOfWork.Save();

                ProductPages.Add(productPage);
            }

            TextName.Text = "";
            TextType.Text = "";
            TextAddress.Text = "";
            TextNode.Text = "";
            TextMatch.Text = "";
        }

        /// <summary>
        /// checks the stock from each product page at the defined interval
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Timer_Tick(object sender, object e)
        {
            _loopCount++;

            TextLoopCount.Text = _loopCount.ToString();
            TextLastRun.Text = DateTime.Now.ToShortTimeString();
            TextNextRun.Text = DateTime.Now.AddMinutes(_interval).ToShortTimeString();

            CheckStock();

            if (_loopCount == _loopLimit)
            {
                _timer.Stop();

                ButtonStockCheck.Content = "Start";
                ComboInterval.IsEnabled = true;
                ComboIterations.IsEnabled = true;
                StackStatistics.Visibility = Visibility.Collapsed;
            }
        }

        #endregion

        #region methods

        /// <summary>
        /// checks the stock rom all product pages
        /// </summary>
        private async void CheckStock()
        {
            foreach (var productPage in ProductPages)
            {
                productPage.Checking = true;

                try
                {
                    var response = await _client.GetAsync(productPage.Address);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var content = await response.Content.ReadAsStringAsync();

                        if (string.IsNullOrEmpty(productPage.Node))
                        {
                            var match = Regex.Match(content, productPage.Match);

                            if (match.Success)
                            {
                                if (productPage.Stock == false)
                                {
                                    productPage.Stock = true;

                                    SendNotification(productPage);
                                }
                            }
                            else
                            {
                                productPage.Stock = false;
                            }
                        }
                        else
                        {
                            var document = new HtmlDocument();
                            document.LoadHtml(content);

                            var node = document.DocumentNode.SelectSingleNode(productPage.Node).OuterHtml.ToString();

                            var match = Regex.Match(node, productPage.Match);

                            if (match.Success)
                            {
                                productPage.Stock = true;

                                SendNotification(productPage);
                            }
                            else
                            {
                                productPage.Stock = false;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    // log error
                }

                productPage.Checking = false;
            }
        }

        /// <summary>
        /// raises a windows notification 
        /// </summary>
        /// <param name="productPage"></param>
        private void SendNotification(ProductPage productPage)
        {
            var toastContent = new ToastContentBuilder()
                                    .AddToastActivationInfo("action=viewConversation&conversationId=5", ToastActivationType.Foreground)
                                    .AddText(productPage.Name)
                                    .AddToastActivationInfo(productPage.Address, ToastActivationType.Foreground)
                                    .AddText(productPage.Address)
                                    .GetToastContent();

            var toast = new ToastNotification(toastContent.GetXml());

            ToastNotificationManager.CreateToastNotifier("Stockr").Show(toast);
        }

        #endregion
    }
}